Slingshot.fileRestrictions("recipeImages", {
  allowedFileTypes: ["image/png", "image/jpeg"],
  maxSize: null
  // maxSize: 10 * 1024 * 1024 // 10 MB (use null for unlimited)
});

Slingshot.fileRestrictions("productImages", {
  allowedFileTypes: ["image/png", "image/jpeg"],
  maxSize: null
  // maxSize: 10 * 1024 * 1024 // 10 MB (use null for unlimited)
});
