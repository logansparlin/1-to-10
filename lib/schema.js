import {Recipes, Products} from './collections'

const Schema = {}

Schema.Image = new SimpleSchema({
  url: {
    type: String,
    optional: true
  },
  uploadedAt: {
    type: String,
    optional: true
  },
  file_name: {
    type: String,
    optional: true
  },
  size: {
    type: String,
    optional: true
  }
})

Schema.Recipes = new SimpleSchema({
  title: {
    type: String,
    label: 'Title',
    optional: false
  },
  slug: {
    type: String,
    label: 'Slug',
    optional: false
  },
  yield: {
    type: String,
    label: 'Yield',
    optional: true
  },
  description: {
    type: String,
    label: 'Description',
    optional: true
  },
  instructions: {
    type: String,
    label: 'Instructions',
    optional: true
  },
  themes: {
    type: [Object],
    optional: true,
    optional: true
  },
  'themes.$.value': {
    type: String,
    optional: true
  },
  'themes.$.label': {
    type: String,
    optional: true
  },
  applications: {
    type: [Object],
    optional: true
  },
  'applications.$.value': {
    type: String,
    optional: true
  },
  'applications.$.label': {
    type: String,
    optional: true
  },
  ingredients: {
    type: [Object],
    optional: true
  },
  'ingredients.$.quantity': {
    type: String,
    optional: true
  },
  'ingredients.$.ingredient': {
    type: String,
    optional: true
  },
  'related_recipes': {
    type: [String],
    optional: true
  },
  'products': {
    type: [String],
    optional: true
  },
  image: {
    type: Schema.Image,
    optional: true
  }
})

Schema.Products = new SimpleSchema({
  title: {
    type: String,
    label: 'Title',
    optional: false
  },
  slug: {
    type: String,
    label: 'Slug',
    optional: false
  },
  sku: {
    type: String,
    label: 'SKU',
    optional: true
  },
  size: {
    type: String,
    label: 'Size',
    optional: true
  },
  description: {
    type: String,
    optional: true
  },
  image: {
    type: Schema.Image,
    optional: true
  }
})

Recipes.attachSchema(Schema.Recipes)
Products.attachSchema(Schema.Products)
