export let Recipes = new Mongo.Collection('recipes')
export let Products = new Mongo.Collection('products')

Recipes.allow({
  insert: () => true,
  update: () => true
})

Products.allow({
  insert: () => true,
  update: () => true
})
