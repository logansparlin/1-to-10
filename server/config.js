import '../lib/s3'

Accounts.config({
  forbidClientAccountCreation : true
});

Slingshot.createDirective("recipeImages", Slingshot.S3Storage, {
  bucket: "files.catering.inspiredflavor.com",

  acl: "public-read",

  authorize: function () {
    // //Deny uploads if user is not logged in.
    // if (!this.userId) {
    //   var message = "Please login before posting files";
    //   throw new Meteor.Error("Login Required", message);
    // }

    return true;
  },

  key: function (file) {
    //Store file into a directory by the user's username.
    // var user = Meteor.users.findOne(this.userId);
    return"recipes/" + file.name;
  }
});

Slingshot.createDirective("productImages", Slingshot.S3Storage, {
  bucket: "files.catering.inspiredflavor.com",

  acl: "public-read",

  authorize: function () {
    // //Deny uploads if user is not logged in.
    // if (!this.userId) {
    //   var message = "Please login before posting files";
    //   throw new Meteor.Error("Login Required", message);
    // }

    return true;
  },

  key: function (file) {
    //Store file into a directory by the user's username.
    // var user = Meteor.users.findOne(this.userId);
    return"products/" + file.name;
  }
});
