import './publications'
import './config'
import './methods'
import '../lib/schema'
import './auth' // Create this file locally and Accounts.createUser
