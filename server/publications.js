import {Recipes, Products} from '../lib/collections'

Meteor.publish('recipes', () => {
  return Recipes.find()
})

Meteor.publish('products', () => {
  return Products.find()
})
