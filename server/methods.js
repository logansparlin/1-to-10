import {Recipes, Products} from '../lib/collections'

Meteor.methods({

  "/recipes/add": function(recipe) {
    console.log(recipe)
    try {
      Recipes.insert(recipe)
    }
    catch(e) {
      throw new Meteor.Error(e.message, e.reason)
    }
  },

  "/recipes/edit": function(recipe) {
    console.log(recipe)
    try {
      Recipes.update({_id: recipe._id}, {$set: { ...recipe }}, {upsert: true})
      return true
    }
    catch(e) {
      throw new Meteor.Error(e.message, e.reason)
    }
  },

  "/recipes/delete": function(id) {
    try {
      Recipes.remove({_id: id})
    }
    catch(e) {
      throw new Meteor.Error(e.message, e.reason)
    }
  },

  "/products/add": function(product) {
    try {
      Products.insert(product)
    }
    catch(e) {
      throw new Meteor.Error(e.message, e.reason)
    }
  },

  "/products/edit": function(product) {
    console.log(product)
    try {
      Products.update({_id: product._id}, {$set: { ...product }}, {upsert: true})
    }
    catch(e) {
      throw new Meteor.Error(e.message, e.reason)
    }
  },

  "/products/delete": function(id) {
    try {
      Products.remove({_id: id})
    }
    catch(e) {
      throw new Meteor.Error(e.message, e.reason)
    }
  }

})
