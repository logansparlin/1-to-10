import React, {Component} from 'react'
import Navigation from './components/Navigation'
import Login from './components/login/Login'
// import 'main.import.styl'

export default class App extends Component {
  render() {
    let{content} = this.props
    if(!Meteor.userId()) {
      return <Login />
    }
    return (
      <div id="catering">
        <Navigation />
        <div className="content-container">
          {content}
        </div>
      </div>
    )
  }
}
