import React, {Component} from 'react'
import DeleteModal from './common/delete-modal/DeleteModal'
// import './List.import.styl'

export default class RecipeList extends Component {

  constructor(props) {
    super(props)

    this.state = {
      deleteModal: false,
      id: null,
      name: null
    }
    this.cancel = this.cancel.bind(this)
    this.remove = this.remove.bind(this)
  }

  promptDelete(id, name) {
    this.setState({
      deleteModal: true,
      id: id,
      name: name
    })
  }

  cancel() {
    this.setState({
      deleteModal: false,
      id: null,
      name: null
    })
  }

  remove() {
    let {id} = this.state;
    Meteor.call('/recipes/delete', id, (e) => {
      if(e) {
        console.log(e)
      } 

      this.setState({
        deleteModal: false,
        id: null,
        name: null
      })
      
    })
  }

  render() {
    let {recipes, loading} = this.props

    if(loading) {
      return <div>Loading</div>
    }

    return (
      <div className="recipes">
        {(this.state.deleteModal) ? <DeleteModal id={this.state.id} name={this.state.name} cancel={this.cancel} remove={this.remove} /> : null }
        <h1 className="main-title">Recipes</h1>
        <ul className="item-list">
          {recipes.map((recipe) => {
            return (
              <li className="single-item" key={recipe._id}>
                
                <div className="inner">
                  <a href={`/recipes/${recipe._id}`}>
                    <div className="fourth">
                      <span className="label title">{recipe.title}</span>
                    </div>
                    <div className="fourth">
                      <span className="label">
                        {recipe.themes.map(theme => {
                          return <span key={theme.value} className="theme">{theme.label}</span>
                        })}
                      </span>
                    </div>
                    <div className="fourth">
                      <span className="label">
                        {recipe.applications.map(application => {
                          return <span key={application.value} className="application">{application.label}</span>
                        })}
                      </span>
                    </div>
                  </a>
                  <div className="actions fourth">
                    <a href={`/recipes/${recipe._id}`}>
                      <i className="fa fa-pencil-square edit"></i>
                    </a>
                    <i className="fa fa-trash delete" onClick={this.promptDelete.bind(this, recipe._id, recipe.title)}></i>
                  </div>
                </div>

              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}
