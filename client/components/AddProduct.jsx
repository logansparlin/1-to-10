import React, {Component} from 'react'
import FileUploader from './common/FileUploader'

const initialState = {
  title: '',
  slug: '',
  sku: '',
  size: '',
  description: '',
  image: {
    url: null,
    file_name: null,
    size: null,
    uploadedAt: null
  },
  error: null
}

export default class AddProduct extends Component {

  constructor(props) {
    super(props)

    this.state = Object.assign({}, initialState, {
      ...props.product
    })

    this.updateField = this.updateField.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.setError = this.setError.bind(this)
  }

  // set state to recipe prop if not received on mount
  componentWillReceiveProps(nextProps) {
    let {product} = nextProps;

    if(product) {
      this.setState(product)
    }
  }

  setError(err) {
    this.setState({
      error: err
    })
  }

  onSubmit(e) {
    e.preventDefault()
    let setError = this.setError;

    let product = _.omit(this.state, 'error')

    if(this.props.edit) {
      Meteor.call('/products/edit', product, (e) => {
        if(e) {
          setError(e.error)
        }
        else {
          FlowRouter.go('/products')
        }
      })
    }
    else {
      Meteor.call('/products/add', product, (e) => {
        if(e) {
          setError(e.error)
        }
        else {
          FlowRouter.go('/products')
        }
      })
    }
  }

  slugify(str) {
    return str.replace(new RegExp(' ', 'g'), '-').toLowerCase()
  }

  updateField(e) {
    let val = e.target.value;
    switch(e.target.id) {
      case "title":
        this.setState({
          title: val,
          slug: this.slugify(val)
        })
        break;
      case "slug":
        this.setState({
          slug: this.slugify(val)
        })
        break;
      case "description":
        this.setState({
          description: val
        })
        break;
      case "sku":
        this.setState({
          sku: val
        })
        break;
      case "size":
        this.setState({
          size: val
        })
        break;
      default:
        console.log('did not change')
        break;
    }
  }

  saveImageUrl(url, file_name, size) {
    let image = {
      url: url,
      file_name: file_name || null,
      uploadedAt: new Date() || null,
      size: size || null
    }
    this.setState({
      image: image
    })
  }

  render() {
    return (
      <div className="add-recipe">
        <h1>Add Product</h1>
          <form onSubmit={this.onSubmit}>
            <FileUploader image={this.state.image} bucket="productImages" saveUrl={this.saveImageUrl.bind(this)}/>
            <input onChange={this.updateField} className="half first" type="text" id="title" placeholder="Title" value={this.state.title} />
            <input onChange={this.updateField} className="half" type="text" id="slug" placeholder="Slug" value={this.state.slug} />
            <input onChange={this.updateField} className="half first" type="text" id="sku" placeholder="SKU" value={this.state.sku} />
            <input onChange={this.updateField} className="half" type="text" id="size" placeholder="Size" value={this.state.size} />
            <textarea onChange={this.updateField} id="description" placeholder="Description" value={this.state.description} />
            {(this.state.error) 
              ? <span className="error">{this.state.error}</span> 
              : null
            }
            <button className="btn save" type="submit">Save</button>
          </form>
      </div>
    )
  }
}
