import React, {Component} from 'react'
// import './AddRecipe.import.styl'
import ThemePicker from './common/ThemePicker'
import ApplicationPicker from './common/ApplicationPicker'
import AddIngredients from './common/AddIngredients'
import FileUploader from './common/FileUploader'
import RelatedRecipes from './common/RelatedRecipes'
import ProductPicker from './common/ProductPicker'
import _ from 'underscore'

const initialState = {
  title: '',
  slug: '',
  yield: '',
  description: '',
  instructions: '',
  image: {
    url: null,
    file_name: null,
    size: null,
    uploadedAt: null
  },
  themes: [],
  applications: [],
  ingredients: [],
  related_recipes: [],
  products: [],
  error: null
}

export default class AddRecipe extends Component {

  constructor(props) {
    super(props)

    this.state = Object.assign({}, initialState, {
      ...props.recipe
    })

    this.changeRelatedRecipe = this.changeRelatedRecipe.bind(this)
    this.changeApplication = this.changeApplication.bind(this)
    this.changeProducts = this.changeProducts.bind(this)
    this.changeTheme = this.changeTheme.bind(this)
    this.updateField = this.updateField.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.setError = this.setError.bind(this)
  }

  // set state to recipe prop if not received on mount
  componentWillReceiveProps(nextProps) {
    if(nextProps.recipe) {
      this.setState(nextProps.recipe)
    }
  }

  changeTheme(val) {
    this.setState({
      themes: val
    })
  }

  changeApplication(val) {
    this.setState({
      applications: val
    })
  }

  changeRelatedRecipe(val) {
    this.setState({
      related_recipes: _.pluck(val, 'value')
    })
  }

  changeProducts(val) {
    this.setState({
      products: _.pluck(val, 'value')
    })
  }

  slugify(str) {
    return str.replace(new RegExp(' ', 'g'), '-').toLowerCase()
  }

  updateIngredients(ingredients) {
    this.setState({
      ingredients
    })
  }

  updateField(e) {
    let val = e.target.value;
    switch(e.target.id) {
      case "title":
        this.setState({
          title: val,
          slug: this.slugify(val)
        })
        break;
      case "slug":
        this.setState({
          slug: this.slugify(val)
        })
        break;
      case "yield":
        this.setState({
          yield: val
        })
        break;
      case "description":
        this.setState({
          description: val
        })
        break;
      case "instructions":
        this.setState({
          instructions: val
        })
        break;
      default:
        console.log('did not change')
        break;
    }
  }

  saveImageUrl(url, file_name, size) {
    let image = {
      url: url,
      file_name: file_name || null,
      uploadedAt: new Date() || null,
      size: size || null
    }
    this.setState({
      image: image
    })
  }

  setError(err) {
    this.setState({
      error: err
    })
  }

  onSubmit(e) {
    e.preventDefault()
    let setError = this.setError;
    let recipe = _.omit(this.state, 'error');

    if(this.props.edit) {
      Meteor.call('/recipes/edit', recipe, (e) => {
        if(e) {
          setError(e.error)
        } else {
          FlowRouter.go('/recipes')
        }
      })
    } else {
      Meteor.call('/recipes/add', recipe, (e) => {
        if(e) {
          setError(e.error)
        } else {
          FlowRouter.go('/recipes')
        }
      })
    }
  }

  render() {
    return (
      <div className="add-recipe">
        <h1>Add Recipe</h1>
        <form onSubmit={this.onSubmit}>
          <FileUploader image={this.state.image} bucket="recipeImages" saveUrl={this.saveImageUrl.bind(this)}/>
          <input onChange={this.updateField} className="half first" type="text" id="title" placeholder="Title" value={this.state.title} />
          <input onChange={this.updateField} className="half" type="text" id="slug" placeholder="Slug" value={this.state.slug} />
          <input onChange={this.updateField} type="text" id="yield" placeholder="Yield" value={this.state.yield} />
          <ThemePicker themes={this.state.themes} onChange={this.changeTheme} />
          <ApplicationPicker applications={this.state.applications} onChange={this.changeApplication} />
          <textarea onChange={this.updateField} id="description" placeholder="Description" value={this.state.description} />
          <textarea onChange={this.updateField} id="instructions" placeholder="Instructions" value={this.state.instructions} />
          <AddIngredients ingredients={this.state.ingredients} updateIngredients={this.updateIngredients.bind(this)} />
          <RelatedRecipes recipes={this.props.recipes} onChange={this.changeRelatedRecipe} selected={this.state.related_recipes} />
          <ProductPicker products={this.props.products} onChange={this.changeProducts} selected={this.state.products} />
          {(this.state.error) 
            ? <span className="error">{this.state.error}</span> 
            : null
          }
          <button className="btn save" type="submit">Save</button>
        </form>
      </div>
    )
  }
}
