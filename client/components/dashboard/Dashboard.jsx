import React, {Component} from 'react'
// import './dashboard.import.styl'

export default class Dashboard extends Component {

  render() {
    let {products, recipes} = this.props;

    return (
      <div id="dashboard">
        <h1 className="main-title" onClick={this.route}>Home</h1>
        <div className="dashboard-container">
          <a href="/recipes">
            <div className="card recipes">
              <div className="inner">
                <span className="count">{recipes}</span>
                <span className="label">Recipes <i className="fa fa-chevron-right right-icon"></i></span>
              </div>
            </div>
          </a>
          <a href="/products">
            <div className="card products">
              <div className="inner">
                <span className="count">{products}</span>
                <span className="label">Products <i className="fa fa-chevron-right right-icon"></i></span>
              </div>
            </div>
          </a>
        </div>
      </div>
    )
  }

}
