import React, {Component} from 'react'
import classNames from 'classnames'
// import './Navigation.import.styl'

const routes = [
  {
    name: "Dashboard",
    path: "/"
  },
  {
    name: "Recipes",
    path: "/recipes",
    add: {
      path: '/recipes/add'
    }
  },
  {
    name: "Products",
    path: "/products",
    add: {
      path: '/products/add'
    }
  }
];

export default class Navigation extends Component {

  logout() {
    Meteor.logout()
  }

  render() {
    let route = FlowRouter.current().route.path

    function classes(path) {
      return classNames('route', {active: (route == path)})
    }

    return (
      <nav className="main-nav">
        <h1>Catering Admin</h1>
        <ul>
          {routes.map((route, index) => {
            return (
              <li key={route.name + index} className={classes(route.path)}>
                <a href={route.path}>{route.name}</a>
                  {(route.add)
                    ? <a  href={route.add.path} className="add-icon">
                        <i className="fa fa-plus plus vertical-center"></i>
                      </a>
                    : ''
                  }
              </li>
            )
          })}
        </ul>
        <div className="logout" onClick={this.logout}>Logout</div>
      </nav>
    )
  }

}
