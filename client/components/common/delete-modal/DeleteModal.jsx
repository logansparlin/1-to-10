import React, {Component} from 'react'
// import './DeleteModal.import.styl'

export default class DeleteModal extends Component {

	render() {
		let {remove, cancel, id, name} = this.props;
		return (
			<div className="delete-modal">
		    <div className="info-box">
		      <h2>{`Are you sure you want to delete ${name}?`}</h2>
		      <button onClick={cancel} className="btn grey half">Cancel</button>
		      <button onClick={remove} className="btn red half">Delete</button>
		    </div>
		  </div>
		)
	}

}

export default DeleteModal