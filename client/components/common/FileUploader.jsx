import React, {Component} from 'react'
// import './FileUploader.import.styl'
import filesize from 'file-size'
import Moment from 'moment'
import '../../../lib/s3'

const initialState = {
  progress: null,
  url: null,
  file_name: null,
  name: null,
  size: null
}

export default class FileUploader extends Component {

  constructor(props) {
    super(props)
    this.state = Object.assign({}, initialState, {
      ...props.image,
      progress: (props.image.url) ? 1 : 0
     })
    this.progressStyles = this.progressStyles.bind(this)
    this.removeImage = this.removeImage.bind(this)
    this.removeElement = this.removeElement.bind(this)
    this.uploadTime = this.uploadTime.bind(this)
  }

  // set state to recipe prop if not received on mount
  componentWillReceiveProps(nextProps) {
    let {image} = nextProps;
    if(image) {
      this.setState(image)
    }
  }

  uploadFile(e) {
    let file = e.target.files[0];

    let {bucket, saveUrl} = this.props;

    let uploader = new Slingshot.Upload(bucket);

    uploader.send(file, function (error, url) {
      if (error) {
        console.error('Error uploading', uploader.xhr.response);
      }
      else {
        
        saveUrl(url, file.name, file.size)
      }
    });

    Tracker.autorun(() => {
      this.setState({
        progress: uploader.progress(),
        url: uploader.url(true),
        file_name: file.name,
        size: file.size
      })
    })
  }

  chooseFile() {
    $('#file-picker').click()
  }

  removeImage() {
    this.setState({
      url: null,
      file_name: null,
      uploadedAt: null,
      progress: null,
      size: null
    })
  }

  progressStyles() {
    let {progress} = this.state;
    return {
      transform: `scaleX(${(progress || 0)})`,
      backgroundColor: (progress == 1) ? 'green' : 'blue'
    }
  }

  getDate(date) {
    return Moment(new Date(date)).startOf('minute').fromNow()
  }

  uploadTime(date) {
    if(date) {
      return (
        <span className="upload-time">
          <i className="fa fa-upload icon"></i>
          {this.getDate(date)}
        </span>
      )
    }
  }

  fileSize(size) {
    if(size) {
      return (
        <span className="file-size">
          {filesize(parseInt(size)).to('MB')} MB
        </span>
      )
    }
  }

  removeElement() {
    if(this.state.url && this.state.progress ==1) {
      return (
        <div className="delete-image" onClick={this.removeImage.bind(this)}>
          <span><i className="fa fa-trash-o"></i>Remove Image</span>
        </div>
      )
    }
  }

  render() {
    let {progress, url, file_name, uploadedAt, size} = this.state;

    return (
      <div className="file-uploader-container">
        <div onClick={this.chooseFile} className="file">
          {(url)
            ? <img className="vertical-center" src={url} />
            : <did className="add-image">
                <span>+ Add Image</span>
              </did>
          }
          <input id="file-picker" onChange={this.uploadFile.bind(this)} type="file" placeholder="Upload Image" />
        </div>
        <div className="file-info">
          <div className="vertical-center">
            <div className="info-header">
              <h4 className="title">{file_name || 'Upload File'}</h4>
              {this.fileSize(size)}
              {this.uploadTime(uploadedAt)}
            </div>
            <div className="progress-bar">
              <div className="progress-bar-inner" style={this.progressStyles()}></div>
            </div>
            {this.removeElement()}
          </div>
        </div>
      </div>
    )
  }
}
