import React, {Component} from 'react'
import Ingredient from './Ingredient'
// import './AddIngredients.import.styl'

export default class AddIngredients extends Component {
  constructor(props) {
    super(props)

    let ingredients = (this.props.ingredients.length >= 1) 
      ? this.props.ingredients 
      : [
          {
            quantity: '',
            ingredient: ''
          }
        ];
    this.state = {
      ingredients: ingredients
    }

    this.addIngredient = this.addIngredient.bind(this)
    this.updateQuantity = this.updateQuantity.bind(this)
    this.updateIngredient = this.updateIngredient.bind(this)
  }

  addIngredient() {
    this.setState({
      ingredients: [
        ...this.state.ingredients,
        {quantity: '', ingredient: ''}
      ]
    })
  }

  deleteIngredient(index) {
    let ingredients = this.state.ingredients
    this.setState({
      ingredients: [
        ...ingredients.slice(0, index),
        ...ingredients.slice(index + 1)
      ]
    })
  }

  updateQuantity(val, index) {
    let newIngredients = this.state.ingredients;
    newIngredients[index].quantity = val

    this.setState({
      ingredients: newIngredients
    })

    this.props.updateIngredients(newIngredients)
  }

  updateIngredient(val, index) {
    let newIngredients = this.state.ingredients;
    newIngredients[index].ingredient = val

    this.setState({
      ingredients: newIngredients
    })
    this.props.updateIngredients(newIngredients)
  }

  render() {
    let {ingredients} = this.state;
    return (
      <div className="ingredients-container">
        <h4>Ingredients</h4>
          <div className="ingredients">
            {ingredients.map((ingredient, index) => {
              return (
                <Ingredient
                  ingredient={ingredient}
                  key={index}
                  index={index}
                  deleteIngredient={this.deleteIngredient.bind(this)}
                  updateQuantity={this.updateQuantity}
                  updateIngredient={this.updateIngredient} />
              )
            })}
          </div>
        <div className="add-ingredient" onClick={this.addIngredient}><i className="fa fa-plus" aria-hidden="true"></i>Add Ingredient</div>
      </div>
    )
  }
}
