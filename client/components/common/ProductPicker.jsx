import React, {Component} from 'react'
import Select from 'react-select'

export default class ProductPicker extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    let {products} = this.props;
    let options = products.map((product) => {
      return {
        value: product._id,
        label: product.title
      }
    })
    return (
      <div className="select-container">
        <Select
          value = {this.props.selected}
          multi={true}
          placeholder="Choose Product(s)"
          onChange={this.props.onChange}
          options={options} />
      </div>
    )
  }

}
