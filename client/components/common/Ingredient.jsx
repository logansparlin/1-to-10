import React, {Component} from 'react'
import {toFraction} from '../../helpers'

export default class Ingredient extends Component {

  constructor(props) {
    super(props)

    this.updateQuantity = this.updateQuantity.bind(this)
    this.updateIngredient = this.updateIngredient.bind(this)
  }

  updateQuantity(e) {
    e.preventDefault()
    let val = toFraction(e.target.value)
    this.props.updateQuantity(val, this.props.index)
  }

  updateIngredient(e) {
    e.preventDefault()
    let val = e.target.value
    this.props.updateIngredient(val, this.props.index)
  }

  render() {
    let {ingredient, index, deleteIngredient} = this.props;
    return (
      <div>
        <div className="delete-ingredient" onClick={deleteIngredient.bind(null, index)}>-</div>
        <div className="ingredient-container" key={index}>
          <input
            onChange={this.updateQuantity}
            className="half first"
            type="text"
            placeholder="Quantity"
            id="quantity"
            value={ingredient.quantity} />
          <input
            onChange={this.updateIngredient}
            className="half"
            type="text"
            placeholder="Ingredient"
            id="ingredient"
            value={ingredient.ingredient} />
        </div>
      </div>
    )
  }
}
