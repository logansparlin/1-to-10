import React, {Component} from 'react'
import Select from 'react-select'

export default class RelatedRecipes extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    let {recipes} = this.props;
    let options = recipes.map((recipe) => {
      return {
        value: recipe._id,
        label: recipe.title
      }
    })
    return (
      <div className="select-container">
        <Select
          value = {this.props.selected}
          multi={true}
          placeholder="Related Recipe(s)"
          onChange={this.props.onChange}
          options={options} />
      </div>
    )
  }

}
