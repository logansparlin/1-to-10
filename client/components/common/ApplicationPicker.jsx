import React, {Component} from 'react'
import Select from 'react-select'

export default class ApplicationPicker extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    let {applications} = this.props

    let options = [
      { value: "spreads", label: "Spreads" },
      { value: "glazes", label: "Glazes" },
      { value: "sauces", label: "Sauces" },
      { value: "dressings-vinaigrettes", label: "Dressings & Vinaigrettes" },
      { value: "dips-toppings", label: "Dips & Toppings" },
      { value: "entrees", label: "Entrées"},
      { value: "sides", label: "Sides" }
    ]

    return (
      <div className="select-container">
        <Select
          value={this.props.applications}
          multi={true}
          placeholder="Application(s)"
          onChange={this.props.onChange}
          options={options} />
      </div>
    )
  }

}
