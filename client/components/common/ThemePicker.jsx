import React, {Component} from 'react'
import Select from 'react-select'

export default class ThemePicker extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    let {themes} = this.props

    let options = [
      { value: "rustic-bbq", label: "Rustic BBQ" },
      { value: "small-bites", label: "Small Bites" },
      { value: "global-flavors", label: "Global Flavors" },
      { value: "entree-salads", label: "Entrée Salads" },
      { value: "tacos-paninis-flatbreads", label: "Tacos/Paninis/Flatbreads" },
      { value: "latin-table", label: "Latin Table" },
      { value: "asian-table", label: "Asian Table" },
      { value: "mediterranean", label: "Mediterranean" }
    ]

    return (
      <div className="select-container">
        <Select
          value={this.props.themes}
          multi={true}
          placeholder="Theme(s)"
          onChange={this.props.onChange}
          options={options} />
      </div>
    )
  }

}
