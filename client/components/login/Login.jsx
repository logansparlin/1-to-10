import React, {Component} from 'react'
// import './login.import.styl'

export default class Login extends Component {
  constructor() {
    super()
    this.login = this.login.bind(this)
  }

  login(e) {
    e.preventDefault()

    let username = this.refs['username'].value;
    let password = this.refs['password'].value;

    Meteor.loginWithPassword(username, password, (e) => {
      if(e) console.log(e)
    })
  }

  render() {
    return (
      <div className="login-container">
        <div className="login-form">
          <h1>Admin Login</h1>
          <form onSubmit={this.login}>
            <input autocorrect="false" autocapitalize="off" spellcheck="false" ref="username" type="text" placeholder="Username" />
            <input autocorrect="false" autocapitalize="off" ref="password" type="password" placeholder="Password" />
            <button className="submit">Login</button>
          </form>
        </div>
      </div>
    )
  }
}
