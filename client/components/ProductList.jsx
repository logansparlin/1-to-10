import React, {Component} from 'react'
import DeleteModal from './common/delete-modal/DeleteModal'
// import './List.import.styl'

export default class ProductList extends Component {

  constructor(props) {
    super(props) 

    this.state = {
      deleteModal: false,
      id: null,
      name: null
    }
    this.cancel = this.cancel.bind(this)
    this.remove = this.remove.bind(this)
  }

  promptDelete(id, name) {
    this.setState({
      deleteModal: true,
      id: id,
      name: name
    })
  }

  cancel() {
    this.setState({
      deleteModal: false,
      id: null,
      name: null
    })
  }

  remove() {
    let {id} = this.state;
    Meteor.call('/products/delete', id, (e) => {
      if(e) {
        console.log(e)
      } 

      this.setState({
        deleteModal: false,
        id: null,
        name: null
      })
      
    })
  }

  render() {
    let {products} = this.props
    return (
      <div className="products">
        { (this.state.deleteModal) 
          ? <DeleteModal id={this.state.id} name={this.state.name} remove={this.remove} cancel={this.cancel} />
          : null
        }
        <h1 className="main-title">Products</h1>
        <ul className="item-list">
          {products.map((product) => {
            return (
              <li className="single-item" key={product._id}>
                <div className="inner">
                  <a href={`/products/${product._id}`}>
                    <div className="fourth">
                        <span className="label title">{product.title}</span>
                    </div>
                  </a>
                  <div className="actions fourth">
                    <a href={`/products/${product._id}`}>
                      <i className="fa fa-pencil-square edit"></i>
                    </a>
                    <i className="fa fa-trash delete" onClick={this.promptDelete.bind(this, product._id, product.title)}></i>
                  </div>
                </div>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}
