export function toFraction(str) {
  let fraction;
  let index = str.indexOf('/')
  if(index !== -1) {
    fraction = str.slice((index - 1), index + 2)
    switch(fraction) {
      case "1/2":
        fraction = "\u00BD"
        break;
      case "1/3":
        fraction = "\u2153"
        break;
      case "1/4":
        fraction = "\u00BC"
        break;
      case "1/5":
        fraction = "\u2155"
        break;
      case "1/8":
        fraction = "\u215B"
        break;
      case "3/8":
        fraction = "\u215C"
        break;
      case "2/5":
        fraction = "\u2156"
        break;
      case "3/5":
        fraction = "\u2157"
        break;
      case "5/8":
        fraction = "\u215D"
        break;
      case "2/3":
        fraction = "\u2154"
        break;
      case "3/4":
        fraction = "\u00BE"
        break;
      case "4/5":
        fraction = "\u2158"
        break;
      case "7/8":
        fraction = "\u215E"
        break;
      default:
        fraction = fraction
    }

    let string = str.slice(0, index - 1) + fraction + str.slice(index + 2)
    return string
  } else {
    return str
  }
}
