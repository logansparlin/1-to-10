import {composeWithTracker} from 'react-komposer'
import App from '../App'

function composer(props, onData) {
  let user = Meteor.userId()
  if(user) {
    onData(null, {loggedIn: true})
  } else {
    onData(null, {loggedIn: false})
  }
}

export default composeWithTracker(composer)(App)
