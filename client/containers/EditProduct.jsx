import {composeWithTracker} from 'react-komposer'
import AddProduct from '../components/AddProduct'
import {Products} from '../../lib/collections'

function composer(props, onData) {
  const handle = Meteor.subscribe('products');
  if(handle.ready) {
    const products = Products.find().fetch()
    const product = _.findWhere(products, {_id: props.productId})
    onData(null, {products, product: product, edit: true})
  } else {
    onData(null, {loading: true})
  }
}

export default composeWithTracker(composer)(AddProduct)
