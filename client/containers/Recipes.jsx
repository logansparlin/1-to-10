import {composeWithTracker} from 'react-komposer'
import RecipeList from '../components/RecipeList'
import {Recipes} from '../../lib/collections'

function composer(props, onData) {
  const handle = Meteor.subscribe('recipes');
  if(handle.ready) {
    const recipes = Recipes.find().fetch()
    onData(null, {recipes})
  } else {
    onData(null, {loading: true})
  }
}

export default composeWithTracker(composer)(RecipeList)
