import {composeWithTracker} from 'react-komposer'
import AddProduct from '../components/AddProduct'
import {Products} from '../../lib/collections'

function composer(props, onData) {
  const handle = Meteor.subscribe('products');
  if(handle.ready) {
    const products = Products.find().fetch()
    onData(null, {products, edit: false})
  } else {
    onData(null, {loading: true})
  }
}

export default composeWithTracker(composer)(AddProduct)
