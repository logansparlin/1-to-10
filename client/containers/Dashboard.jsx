import {composeWithTracker} from 'react-komposer'
import Dashboard from '../components/dashboard/Dashboard'
import {Recipes, Products} from '../../lib/collections'

function composer(props, onData) {
  const products = Meteor.subscribe('products');
  const recipes = Meteor.subscribe('recipes')
  if(products.ready && recipes.ready) {
    const recipeCount = Recipes.find().count()
    const productCount = Products.find().count()
    onData(null, {recipes: recipeCount, products: productCount})
  } else {
    onData(null, {loading: true})
  }
}

export default composeWithTracker(composer)(Dashboard)
