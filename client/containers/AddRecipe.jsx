import {composeWithTracker} from 'react-komposer'
import AddRecipe from '../components/AddRecipe'
import {Recipes, Products} from '../../lib/collections'

function composer(props, onData) {
  const recipeHandle = Meteor.subscribe('recipes');
  const productHandle = Meteor.subscribe('products');
  if(recipeHandle.ready && productHandle.ready) {
    const recipes = Recipes.find().fetch()
    const products = Products.find().fetch()
    onData(null, {recipes, products, edit: false})
  } else {
    onData(null, {loading: true})
  }
}

export default composeWithTracker(composer)(AddRecipe)
