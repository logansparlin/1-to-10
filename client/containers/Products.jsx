import {composeWithTracker} from 'react-komposer'
import ProductList from '../components/ProductList'
import {Products} from '../../lib/collections'

function composer(props, onData) {
  const handle = Meteor.subscribe('products');
  if(handle.ready) {
    const products = Products.find().fetch()
    onData(null, {products})
  } else {
    onData(null, {loading: true})
  }
}

export default composeWithTracker(composer)(ProductList)
