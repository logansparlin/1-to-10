import React from 'react'
import {mount} from 'react-mounter'

import Dashboard from './containers/Dashboard'
import Recipes from './containers/Recipes'
import App from './containers/AppContainer'
import Products from './containers/Products'
import AddRecipe from './containers/AddRecipe'
import AddProduct from './containers/AddProduct'
import EditRecipe from './containers/EditRecipe'
import EditProduct from './containers/EditProduct'

FlowRouter.route('/', {
  name: 'home',
  action() {
    mount(App, {
      content: <Dashboard />
    })
  }
})

FlowRouter.route('/recipes', {
  name: 'recipes',
  action() {
    mount(App, {
      content: <Recipes />
    })
  }
})

FlowRouter.route('/products', {
  name: 'products',
  action() {
    mount(App, {
      content: <Products />
    })
  }
})

FlowRouter.route('/products/add', {
  name: 'add-product',
  action() {
    mount(App, {
      content: <AddProduct />
    })
  }
})

FlowRouter.route('/recipes/add', {
  name: 'add-recipe',
  action() {
    mount(App, {
      content: <AddRecipe />
    })
  }
})

FlowRouter.route('/recipes/:id', {
  name: 'edit-recipe',
  action(params) {
    mount(App, {
      content: <EditRecipe recipeId={params.id} />
    })
  }
})

FlowRouter.route('/products/:id', {
  name: 'edit-product',
  action(params) {
    mount(App, {
      content: <EditProduct productId={params.id} />
    })
  }
})
